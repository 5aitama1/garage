using System;

namespace Garage {
    public abstract class Transport : Vehicule 
    {
        public int Tonnage { get; protected set; }

        public Transport (string plaque) : base (plaque) 
        { }

        public void Charger () 
        {
            Console.WriteLine ("Le véhicule de transport est chargé !");
        }

        public void Decharger () 
        {
            Console.WriteLine ("Le véhicule de transport est déchargé !");
        }
    }
}
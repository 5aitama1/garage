using System;

namespace Garage
{
    public class Camion : Transport
    {
        private int _nombreRoues;
        private int _tonnage;

        public Camion(string plaque) : base(plaque)
        { }

        public override void Rouler()
        {
            Console.WriteLine("Le camion roule !");
        }

        public override void Freiner()
        {
            Console.WriteLine("Le camion freine !");
        }

        public override void Accelerer()
        {
            Console.WriteLine("Le camion accélère !");
        }
    }
}